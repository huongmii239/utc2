import 'package:flutter/material.dart';

class Product {
  final int id;
  final String title, description, price;
  final List<String> images;
  final List<Color> colors;
  final double rating;
  final bool isFavourite, isPopular;

  Product({
    required this.id,
    required this.images,
    required this.colors,
    this.rating = 0.0,
    this.isFavourite = false,
    this.isPopular = false,
    required this.title,
    required this.price,
    required this.description,
  });
}

// Our demo Products

List<Product> demoProducts = [
  Product(
    id: 1,
    images: [
      "assets/images/ps4_console_white_1.png",
      "assets/images/ps4_console_white_2.png",
      "assets/images/ps4_console_white_3.png",
      "assets/images/ps4_console_white_4.png",
    ],
    colors: [
      const Color(0xFFF6625E),
    ],
    title: "Bộ bàn ăn Tristan Belfast 6 chỗ",
    price: "22,490.000",
    description:
        "Bàn: D1600-R900-C750\nGhế: D540-R485-C830 mm,\nChất liệu: Gỗ - MDF VENEER ASH -Nệm bọc vải",
    rating: 4.8,
    isFavourite: true,
    isPopular: true,
  ),
  Product(
    id: 2,
    images: [
      "assets/images/Image Popular Product 2.png",
      "assets/images/Product2_1.png",
      "assets/images/Product2_2.png",
      "assets/images/Product2_3.png"
    ],
    colors: [
      const Color(0xFFCB504B),
      const Color(0xFF0D1D66),
    ],
    title: "Bàn nước Glam Phòng khách",
    price: "19.540.000",
    description: "Ø600 - C350 mm\nChất liệu: Kim loại sơn màu",
    rating: 4.1,
    isPopular: true,
  ),
  Product(
    id: 3,
    images: [
      "assets/images/Image Popular Product 3.png",
      "assets/images/Product3_1.png"
    ],
    colors: [
      const Color(0xFF443A25),
    ],
    title: "Đồng hồ để bàn Moto kim loại",
    price: "2,100,001",
    description: "D320 - R65 - C190 mm\nChất liệu: Kim loại",
    rating: 4.1,
    isPopular: true,
  ),
  Product(
    id: 4,
    images: [
      "assets/images/Image Popular Product 4.png",
      "assets/images/Product4_1.png"
    ],
    colors: [
      const Color(0xFF76684D),
    ],
    title: "Kệ Sách Artigo Pháp Gautier",
    price: "29,360,000",
    description:
        "D850 - R380 - C1980mm\nChất liệu: Gỗ Sồi kết hợp MDF veneer\nchân thép sơn tĩnh điện\nXuất xứ: Pháp",
    rating: 4.9,
    isFavourite: true,
    isPopular: true,
  ),
];
