import 'package:flutter/material.dart';
import 'package:my_pth_huong/components/coustom_bottom_nav_bar.dart';
import 'package:my_pth_huong/constants.dart';
import 'package:my_pth_huong/screens/profile/components/body_profile.dart';

class ProfileScreen extends StatelessWidget {
  static String routeName = "/profile";

  const ProfileScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: const Text("Profile"),
      ),
      body: const BodyProfile(),
      bottomNavigationBar:
          const CustomBottomNavBar(selectedMenu: MenuState.profile),
    );
  }
}
