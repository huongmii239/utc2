import 'package:flutter/material.dart';
import 'package:my_pth_huong/constants.dart';
import 'package:my_pth_huong/size_config.dart';

import 'complete_profile_form.dart';

class BodyCompleteProfile extends StatelessWidget {
  const BodyCompleteProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: SizeConfig.screenHeight * 0.03),
                Text("Hoàn thành Hồ sơ", style: headingStyle),
                const Text(
                  "Hoàn thành thông tin cá nhân\nHoặc tiếp tục bằng Mạng xã hội",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: SizeConfig.screenHeight * 0.06),
                const CompleteProfileForm(),
                SizedBox(height: getProportionateScreenHeight(30)),
                Text(
                  "Bằng cách tiếp tục xác nhận rằng bạn đồng ý với\nĐiều khoản và Điều kiện của chúng tôi",
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
