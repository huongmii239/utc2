import 'package:flutter/material.dart';
import 'package:my_pth_huong/components/coustom_bottom_nav_bar.dart';
import 'package:my_pth_huong/constants.dart';
import 'package:my_pth_huong/screens/list/components/body_list.dart';

class ListScreen extends StatelessWidget {
  static String routeName = "/favourite";

  const ListScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: kPrimaryColor,
        centerTitle: true,
        title: const Text("Danh mục tham khảo"),
      ),
      body: const BodyList(),
      bottomNavigationBar:
          const CustomBottomNavBar(selectedMenu: MenuState.favourite),
    );
  }
}
