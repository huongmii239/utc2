import 'package:flutter/material.dart';
import 'package:my_pth_huong/components/default_button.dart';
import 'package:my_pth_huong/constants.dart';
import 'package:my_pth_huong/screens/sign_in/sign_in_screen.dart';
import 'package:my_pth_huong/screens/splash/components/splash_content.dart';
import 'package:my_pth_huong/size_config.dart';

class BodySplash extends StatefulWidget {
  const BodySplash({Key? key}) : super(key: key);

  @override
  State<BodySplash> createState() => _BodySplashState();
}

class _BodySplashState extends State<BodySplash> {
  int currentPage = 0;
  List<Map<String, String>> splashList = [
    {
      "title": "Chào mừng đến với Phú Thịnh Homes.\nHãy mua sắm thôi!",
      "picture": "assets/images/splash_1.png"
    },
    {
      "title":
          "Chúng tôi giúp mọi người kết nối với cửa hàng\ntại mọi nơi tại Việt Nam",
      "picture": "assets/images/splash_2.png"
    },
    {
      "title": "Mua sắm một cách dễ dàng.\nChỉ cần một cú click ngay tại nhà",
      "picture": "assets/images/splash_3.png"
    }
  ];

  AnimatedContainer buildDot({int? index}) {
    return AnimatedContainer(
      duration: kAnimationDuration,
      margin: const EdgeInsets.only(right: 5),
      height: 6,
      width: currentPage == index ? 20 : 6,
      decoration: BoxDecoration(
        color: currentPage == index ? kPrimaryColor : const Color(0xFFD8D8D8),
        borderRadius: BorderRadius.circular(3),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Column(
          children: [
            Expanded(
              flex: 3,
              child: PageView.builder(
                onPageChanged: (value) {
                  setState(() {
                    currentPage = value;
                  });
                },
                itemCount: splashList.length,
                itemBuilder: (context, index) => SplashContent(
                  image: splashList[index]["picture"],
                  text: splashList[index]['title'],
                ),
              ),
            ),
            Expanded(
              flex: 2,
              child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: getProportionateScreenWidth(20)),
                child: Column(
                  children: <Widget>[
                    const Spacer(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: List.generate(
                        splashList.length,
                        (index) => buildDot(index: index),
                      ),
                    ),
                    const Spacer(flex: 3),
                    DefaultButton(
                      text: "Tiếp tục",
                      press: () {
                        Navigator.pushNamed(context, SignInScreen.routeName);
                      },
                    ),
                    const Spacer(),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
