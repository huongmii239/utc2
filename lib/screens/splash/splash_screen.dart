import 'package:flutter/material.dart';
import 'package:my_pth_huong/screens/splash/components/body_splash.dart';
import 'package:my_pth_huong/size_config.dart';

class SplashScreen extends StatelessWidget {
  static String routeName = "/splash";

  const SplashScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return const Scaffold(
      body: BodySplash(),
    );
  }
}
