import 'package:flutter/material.dart';
import 'package:my_pth_huong/screens/home/components/categories.dart';
import 'package:my_pth_huong/screens/home/components/discount_banner.dart';
import 'package:my_pth_huong/screens/home/components/home_header.dart';
import 'package:my_pth_huong/screens/home/components/popular_product.dart';
import 'package:my_pth_huong/screens/home/components/special_offers.dart';
import 'package:my_pth_huong/size_config.dart';

class BodyHome extends StatelessWidget {
  const BodyHome({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: [
            SizedBox(height: getProportionateScreenHeight(20)),
            const HomeHeader(),
            SizedBox(height: getProportionateScreenWidth(10)),
            const DiscountBanner(),
            const Categories(),
            const SpecialOffers(),
            SizedBox(height: getProportionateScreenWidth(30)),
            const PopularProducts(),
            SizedBox(height: getProportionateScreenWidth(30)),
          ],
        ),
      ),
    );
  }
}
