import 'package:flutter/material.dart';
import 'package:my_pth_huong/components/coustom_bottom_nav_bar.dart';
import 'package:my_pth_huong/constants.dart';
import 'package:my_pth_huong/screens/home/components/body_home.dart';

class HomeScreen extends StatelessWidget {
  static String routeName = "/home";

  const HomeScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: BodyHome(),
      bottomNavigationBar: CustomBottomNavBar(selectedMenu: MenuState.home),
    );
  }
}
