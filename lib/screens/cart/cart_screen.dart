import 'package:flutter/material.dart';
import 'package:my_pth_huong/models/Cart.dart';
import 'package:my_pth_huong/screens/cart/components/body_cart.dart';
import 'package:my_pth_huong/screens/cart/components/check_out_card.dart';

class CartScreen extends StatefulWidget {
  static String routeName = "/cart";

  const CartScreen({Key? key}) : super(key: key);

  @override
  State<CartScreen> createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: buildAppBar(context),
      body: const BodyCart(),
      bottomNavigationBar: const CheckoutCard(),
    );
  }

  AppBar buildAppBar(BuildContext context) {
    return AppBar(
      title: Column(
        children: [
          const Text(
            "Giỏ hàng của bạn",
            style: TextStyle(color: Colors.black),
          ),
          Text(
            "${demoCarts.length} mặt hàng",
            style: Theme.of(context).textTheme.caption,
          ),
        ],
      ),
    );
  }
}
