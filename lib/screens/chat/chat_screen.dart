import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:my_pth_huong/components/coustom_bottom_nav_bar.dart';
import 'package:my_pth_huong/constants.dart';
import 'package:my_pth_huong/screens/chat/components/body_chat.dart';

class ChatScreen extends StatelessWidget {
  static String routeName = "/message";

  const ChatScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: SvgPicture.asset("assets/icons/Chatbot Icon.svg"),
          onPressed: () {},
        ),
        backgroundColor: kPrimaryColor,
        centerTitle: true,
        title: const Text("Tin nhắn tự động"),
      ),
      body: const BodyChat(),
      bottomNavigationBar:
          const CustomBottomNavBar(selectedMenu: MenuState.message),
    );
  }
}
