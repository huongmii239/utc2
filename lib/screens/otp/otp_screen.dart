import 'package:flutter/material.dart';
import 'package:my_pth_huong/screens/otp/components/body.dart';
import 'package:my_pth_huong/size_config.dart';

class OtpScreen extends StatelessWidget {
  static String routeName = "/otp";

  const OtpScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text("Xác minh OTP"),
      ),
      body: const BodyOTP(),
    );
  }
}
