import 'package:flutter/material.dart';
import 'package:my_pth_huong/screens/sign_up/components/body_sign_up.dart';

class SignUpScreen extends StatelessWidget {
  static String routeName = "/sign_up";

  const SignUpScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Đăng ký"),
      ),
      body: const BodySignUp(),
    );
  }
}
