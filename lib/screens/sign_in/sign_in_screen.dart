import 'package:flutter/material.dart';
import 'package:my_pth_huong/screens/sign_in/component/body_sign_in.dart';

class SignInScreen extends StatelessWidget {
  static String routeName = "/sign_in";

  const SignInScreen({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Đăng nhập"),
      ),
      body: const BodySignIn(),
    );
  }
}
