import 'package:flutter/material.dart';
import 'package:my_pth_huong/screens/cart/cart_screen.dart';
import 'package:my_pth_huong/screens/chat/chat_screen.dart';
import 'package:my_pth_huong/screens/complete_profile/complete_profile_screen.dart';
import 'package:my_pth_huong/screens/details/details_screen.dart';
import 'package:my_pth_huong/screens/forgot_password/forgot_pass_screen.dart';
import 'package:my_pth_huong/screens/home/home_screen.dart';
import 'package:my_pth_huong/screens/list/list_screen.dart';
import 'package:my_pth_huong/screens/login_success/login_success_screen.dart';
import 'package:my_pth_huong/screens/otp/otp_screen.dart';
import 'package:my_pth_huong/screens/profile/profile_screen.dart';
import 'package:my_pth_huong/screens/sign_in/sign_in_screen.dart';
import 'package:my_pth_huong/screens/sign_up/sign_up_screen.dart';
import 'package:my_pth_huong/screens/splash/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => const SplashScreen(),
  SignInScreen.routeName: (context) => const SignInScreen(),
  ForgotPassScreen.routeName: (context) => const ForgotPassScreen(),
  LoginSuccessScreen.routeName: (context) => const LoginSuccessScreen(),
  SignUpScreen.routeName: (context) => const SignUpScreen(),
  CompleteProfileScreen.routeName: (context) => const CompleteProfileScreen(),
  OtpScreen.routeName: (context) => const OtpScreen(),
  HomeScreen.routeName: (context) => const HomeScreen(),
  DetailsScreen.routeName: (context) => const DetailsScreen(),
  CartScreen.routeName: (context) => CartScreen(),
  ProfileScreen.routeName: (context) => const ProfileScreen(),
  ChatScreen.routeName: (context) => const ChatScreen(),
  ListScreen.routeName: (context) => const ListScreen(),
};
